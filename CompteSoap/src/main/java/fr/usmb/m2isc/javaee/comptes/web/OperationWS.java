package fr.usmb.m2isc.javaee.comptes.web;

import java.util.List;

import jakarta.ejb.EJB;
import jakarta.jws.WebMethod;
import jakarta.jws.WebService;

import fr.usmb.m2isc.javaee.comptes.ejb.Operation;
import fr.usmb.m2isc.javaee.comptes.jpa.Compte;

@WebService(serviceName = "OperationService")
public class OperationWS {
	@EJB
	private Operation ejb;

	@WebMethod
	public Compte creerCompte(String number, double depot) {
		return ejb.creerCompte(number, depot);
	}

	@WebMethod
	public Compte getCompte(String number) {
		return ejb.getCompte(number);
	}

	@WebMethod
	public Compte crediter(String number, double val) {
		return ejb.crediter(number, val);
	}

	@WebMethod
	public Compte debiter(String number, double val) {
		return ejb.debiter(number, val);
	}

	@WebMethod
	public void transferer(String numCpt1, double val, String numCpt2) {
		ejb.transferer(numCpt1, val, numCpt2);
	}

	@WebMethod
	public List<Compte> findComptes(String partialNumber) {
		return ejb.findComptes(partialNumber);
	}

	@WebMethod
	public List<Compte> findAllComptes() {
		return ejb.findAllComptes();
	}
}
