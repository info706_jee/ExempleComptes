package fr.usmb.m2isc.javaee.comptes.web;

import java.io.IOException;
import java.util.List;

import jakarta.ejb.EJB;
import jakarta.servlet.ServletException;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

import fr.usmb.m2isc.javaee.comptes.ejb.Operation;
import fr.usmb.m2isc.javaee.comptes.jpa.Compte;


@Path("bank")
public class OperationRest {
	@EJB
	private Operation ejb;
	
	public OperationRest() {
		super();
	}

	@POST
	@Path("compte")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Compte creerCompte(Compte cpt) {
		return ejb.creerCompte(cpt.getNumero(), cpt.getSolde());
	}

	@GET
	@Path("compte/{number}")
	@Produces(MediaType.APPLICATION_JSON)
	public Compte getCompte(@PathParam("number") String number) {
		System.out.println(number);
		Compte cpt = ejb.getCompte(number);
		System.out.println("xxxxx" + cpt);		
		return cpt;
	}

	@GET
	@Path("compte/{number}/crediter")
	@Produces(MediaType.APPLICATION_JSON)
	public Compte crediter(@PathParam("number")String number, @QueryParam("montant") double val) {
		return ejb.crediter(number, val);
	}

    @GET
    @Path("compte/{number}/debiter")
    @Produces(MediaType.APPLICATION_JSON)
    public Compte debiter(@PathParam("number")String number, @QueryParam("montant") double val) {
		return ejb.debiter(number, val);
	}

	@GET
	@Path("comptes/{partialNumber}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Compte> findComptes(@PathParam("partialNumber") String partialNumber) throws ServletException, IOException {
		 List<Compte> listeCpt = ejb.findComptes("%"+partialNumber+"%");
		 return listeCpt;
	}

	@GET
	@Path("comptes")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Compte> findAllComptes() {
		return ejb.findAllComptes();
	}
}
