package fr.usmb.m2isc.javaee.comptes.web;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;

@ApplicationPath("/bankApi/v0.1.0")
public class AppBank extends Application {

}
