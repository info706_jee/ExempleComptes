# Un exemple minimaliste d'une application Jakarta EE 9

Pour faire simple l'application consiste à manipuler des comptes bancaires (très simplifiés).

Un compte est identifié par un numéro (alphanumérique) et le montant du compte.

On peut :
- créer des comptes,
- débiter ou créditer un compte et
- transférer de l'argent d'un compte vers un autre.

Les comptes sont stockés dans une base de données. Une entité (JPA) permet d'y accéder (cf. partie EJBs).

Un EJB session sans état (Stateless) sert de façade pour les opérations bancaires.

Un client WEB permet de réaliser l'ensemble des opérations sur les comptes : création, débit, crédit, transfert, visualisation ou recherche (cf. client Web). 
Ce client comporte deux pages jsp et plusieurs servlet (1 par opération). La connexion au bean session se fait à l'aide de son interface distante.

## Partie EJB

L'objet persistant, Compte, ainsi que l'EJB session et son interface sont regroupés ensembles dans la même archive.
<pre>
CompteEjb.jar (ejb-jar)
  |-- <a href="CompteEjb/src/main/java/fr/usmb/m2isc/javaee/comptes/jpa/Compte.java" >fr/usmb/m2isc/.../jpa/Compte.class</a> (implantation de l'entité Compte (entité JPA))
  |-- <a href="CompteEjb/src/main/java/fr/usmb/m2isc/javaee/comptes/ejb/OperationBean.java" >fr/usmb/m2isc/.../ejb/OperationBean.class</a> (implantation du l'EJB Operation (bean session))
  |-- <a href="CompteEjb/src/main/java/fr/usmb/m2isc/javaee/comptes/ejb/Operation.java" >fr/usmb/m2isc/.../ejb/Operation.class</a> (interfaces de manipulation distante du bean session)
  |-- META-INF/MANIFEST.MF (java manifeste)
  |-- <a href="CompteEjb/src/main/resources/META-INF/ejb-jar.xml" >META-INF/ejb-jar.xml</a> (descripteur standard des Jakarta Enterprise Beans (EJB) -- optionnel dans les dernières versions de javaEE et pour Jakarta EE)
  |-- <a href="CompteEjb/src/main/resources/META-INF/persistence.xml" >META-INF/persistence.xml</a> (descripteur standard pour JPA)
  |-- META-INF/orm.xml (descripteur pour le mapping objet-relationnel -- absent ici)
</pre>

Toutes les manipulations sur les objets persistants se font dans l'EJB en utilisant l'*entity manager* correspondant à l'*unité de persistance* des objets persistants manipulés. 

Dans l'EJB on utilise l'annotation `@PersistenceContext` pour récupérer auprès du serveur Jakarta EE (ou Java EE) l'*entity manager* désiré.

```java
@Stateless
@Remote
public class OperationBean implements Operation {
	
	@PersistenceContext
	private EntityManager em;
```

Pour les opérations de recherche de comptes, sur l'entité JPA ont été ajoutés deux requetes nommées

```java
@NamedQueries ({
	@NamedQuery(name="all", query="SELECT c FROM Compte c"),
	@NamedQuery(name="findWithNum", query="SELECT c FROM Compte c WHERE c.numero LIKE :partialNum ORDER BY c.numero ASC")
})
@Entity
public class Compte implements Serializable {
	...
```

utilisées dans l'EJB session pour obtenir soit la liste complète des comptes, soit une partie d'entre eux :

```
@Stateless
@Remote
public class OperationBean implements Operation {	
	...	
	@Override
	public List<Compte> findAllComptes() {
		Query req = em.createNamedQuery("all");
		return req.getResultList();
	}

	@Override
	public List<Compte> findComptes(String partialNumber) {
		Query req = em.createNamedQuery("findWithNum");
		req.setParameter("partialNum", partialNumber);
		return req.getResultList();
	}	
	...
```

 
## L'application WEB est dans un fichier d'archive war :

Ce client WEB permet de réaliser l'ensemble des opérations sur les comptes : création, débit, crédit, transfert, recherche ou visualisation.

Ce client comporte des pages jsp et des servlet. La connexion au bean session se fait à l'aide de l'interface distante de l'EJB. 
On utilise les *servlet* pour traiter les requêtes et les *pages JSP* pour l'affichage du résultat.
<pre>
CompteWeb.war
  |-- <a href="CompteWeb/src/main/webapp/index.html" >index.html</a> (page d'accueil -- formulaires html permettant de créer, rechercher ou modifier les comptes)
  |-- <a href="CompteWeb/src/main/webapp/AfficherCompte.jsp" >AfficherCompte.jsp</a> (page jsp pour afficher un compte)
  |-- <a href="CompteWeb/src/main/webapp/AfficherCompte.jsp" >AfficherComptes.jsp</a> (page jsp pour afficher plusieurs comptes)
  |-- <a href="CompteWeb/src/main/webapp/META-INF/MANIFEST.MF" >META-INF/MANIFEST.MF</a> (java manifeste)
  |-- WEB-INF/classes (classes java pour les servlets :
                |-- <a href="CompteWeb/src/main/java/fr/usmb/m2isc/javaee/comptes/web/CreerCompteServlet.java" >fr/usmb/m2isc/javaee/comptes/web/CreerCompteServlet.class</a>
                |-- <a href="CompteWeb/src/main/java/fr/usmb/m2isc/javaee/comptes/web/CrediterCompteServlet.java" >fr/usmb/m2isc/javaee/comptes/web/CrediterCompteServlet.class</a>
                |-- <a href="CompteWeb/src/main/java/fr/usmb/m2isc/javaee/comptes/web/DebiterCompteServlet.java" >fr/usmb/m2isc/javaee/comptes/web/DebiterCompteServlet.class</a>
                |-- <a href="CompteWeb/src/main/java/fr/usmb/m2isc/javaee/comptes/web/TransfererServlet.java" >fr/usmb/m2isc/javaee/comptes/web/TransfererServlet.class</a>
                |-- <a href="CompteWeb/src/main/java/fr/usmb/m2isc/javaee/comptes/web/ChercherComptesServlet.java" >fr/usmb/m2isc/javaee/comptes/web/ChercherComptesServlet.class</a>
                |-- <a href="CompteWeb/src/main/java/fr/usmb/m2isc/javaee/comptes/web/AfficherCompteServlet.java" >fr/usmb/m2isc/javaee/comptes/web/AfficherCompteServlet.class</a>
  |-- WEB-INF/lib (librairies java utilisées dans les servlet)
  |-- <a href="CompteWeb/src/main/webapp/WEB-INF/web.xml" >WEB-INF/web.xml</a> (descripteur standard de l'application Web -- optionnel dans les dernières versions de javaEE et pour Jakarta EE)
</pre>

Dans les *servlet* on utilise l'annotation `@EJB` pour obtenir une référence de l'*EJB session* :

```java
@WebServlet("/CreerCompteServlet")
public class CreerCompteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
	private Operation ejb;
```

L'EJB est ensuite utilisé par les servlet pour effectuer les traitements :

```java
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// recuperation des parametres de la requete
		String num = request.getParameter("numero");
		String val = request.getParameter("depot");
		double depot = Double.parseDouble(val);
		
		// utilisation de l'EJB
		Compte cpt = ejb.creerCompte(num, depot);
		...
```

puis on utilise la requête pour passer les objets à afficher à la *page JSP* chargée de l'affichage :

```java
		...
		// ajout du compte dans la requete
		request.setAttribute("compte", cpt);
		// redirection vers la page JSP pour afficher le compte
		request.getRequestDispatcher("/AfficherCompte.jsp").forward(request, response);		
	}
```

## Service Web SOAP

Java EE / Jakarta EE permet de transformer un EJB Session sans état (ou une classe Java) en service Web de type SOAP (ancien acronyme de Simple Object Access Protocol). 

À cet effet, il suffit d'annoter votre EJB (ou votre classe java) avec l'annotation `@WebService` 
ainsi que les méthodes que vous souhaitez faire apparaître dans votre service WEB avec l'annotation `@WebMethod`.

Le serveur JEE implantera alors le service Web SOAP correspondant et publiera sa description sous forme de fichier [`wsdl`](https://fr.wikipedia.org/wiki/Web_Services_Description_Language).

Pour l'application de comptes bancaires, nous avons implanté le service WEB de manipulation des comptes dans une application web séparée : [`CompteSoap`](CompteSoap).  
Le service WEB est fourni par la classe [`OperationWS`](CompteSoap/src/main/java/fr/usmb/m2isc/javaee/comptes/web/OperationWS.java) :

```java
@WebService(serviceName = "OperationService")
public class OperationWS {
	@EJB
	private Operation ejb;

	@WebMethod
	public Compte creerCompte(String number, double depot) {
		return ejb.creerCompte(number, depot);
	}

	@WebMethod
	public Compte getCompte(String number) {
		return ejb.getCompte(number);
	}

	@WebMethod
	public Compte crediter(String number, double val) {
		return ejb.crediter(number, val);
	}

	@WebMethod
	public Compte debiter(String number, double val) {
		return ejb.debiter(number, val);
	}

	@WebMethod
	public void transferer(String numCpt1, double val, String numCpt2) {
		ejb.transferer(numCpt1, val, numCpt2);
	}

	@WebMethod
	public List<Compte> findComptes(String partialNumber) {
		return ejb.findComptes(partialNumber);
	}

	@WebMethod
	public List<Compte> findAllComptes() {
		return ejb.findAllComptes();
	}
}
```

Une fois déployé sur un serveur Jakarta EE 9 (ou Jakarta EE 10), 
comme [Payara 6](https://www.payara.fish/downloads/payara-platform-community-edition/) 
ou [WildFly 27+](https://www.wildfly.org/downloads/), 
la description du service est alors accessible 
à l'url http://localhost:8080/CompteSoap/OperationService?wsdl et 
le service web à l'url [http://localhost:8080/CompteSoap/OperationService](http://localhost:8080/CompteSoap/OperationService).

À noter que les services Web SOAP sont devenus une spécification optionnelle de Jakarta EE avec la version Jakarta EE 9.

À noter également que Oracle a supprimé les packages permettant le support des services Web SOAP de la JDK à partir de la JDK 11. 
Il faut donc ajouter une dépendance vers `jakarta.jws:jakarta.jws-api:3.0.0` dans le [`build.gradle`](CompteSoap/build.gradle)
pour pouvoir compiler l'exemple avec une JDK-11 ou une JDK-17.

## Service Web Rest

Depuis JavaEE 6 et Java EE 7, il est également possible de créer des service Web REST (XML ou JSON). 
Un exemple est fourni dans l'application web [`CompteRest`](CompteRest).

La classe [`OperationRest`](CompteRest/src/main/java/fr/usmb/m2isc/javaee/comptes/web/OperationRest.java) fournit l'implantation de l'API REST, 
tandis que la classe [`AppBank`](CompteRest/src/main/java/fr/usmb/m2isc/javaee/comptes/web/AppBank.java) permet de se dispenser de configurer la Servlet 
permettant mettre en œuvre la partie Web du service Web REST. 

Une fois déployé sur un serveur Jakarta EE 9 (ou Jakarta EE 10), 
comme [Payara 6](https://www.payara.fish/downloads/payara-platform-community-edition/) 
ou [WildFly 27+](https://www.wildfly.org/downloads/), 
le service web devrait être accessible sur des url commençant par `http://localhost:8080/CompteRest/bankApi/v0.1.0/bank/`.
Par exemple, une requête `GET` vers http://localhost:8080/CompteRest/bankApi/v0.1.0/bank/compte/cpt1234 
devrait permettre d'obtenir la représention, au format JSON, du compte de numéro `cpt1234`

Remarque : afin que l'on puisse injecter l'EJB dans les instances de la classe 
[`OperationRest`](CompteRest/src/main/java/fr/usmb/m2isc/javaee/comptes/web/OperationRest.java),
celle-ci doit être vue, par le serveur Jakarta EE, comme un [`Bean CDI` (ou un `managed bean`)](https://jakarta.ee/specifications/cdi/).  
Dans cet objectif l'ajout du fichier (vide) [WEB-INF/beans.xml](CompteRest/src/main/webapp/WEB-INF/beans.xml) 
permet d'activer l'utilisation de CDI pour le projet Web. 


## Le tout est packagé ensemble dans une archive ear :

Cette archive permet de regrouper dans le même fichier l'ensemble des composants de l'application (ejb, app web, etc.).
<pre>
CompteEar.ear
  |-- CompteEjb.jar (archive contenant les EJBs)
  |-- CompteWeb.war (archive contenant le client Web)
  |-- CompteSoap.war (archive contenant le service Web SOAP)
  |-- CompteRest.war (archive contenant le service Web REST)
  |-- <a href="CompteEar/src/main/application/META-INF/application.xml" >META-INF/application.xml</a> (descripteur standard de l'application -- optionnel dans les dernières versions de javaEE)
</pre>

## Usage :

Pour voir les sources il suffit de cloner le projet git et de l'importer (sous forme de projet gradle) dans votre IDE favori. 
Cela devrait permettre la création de 5 sous-projets (ou modules), un pour la partie EJB et JPA, un pour la partie WEB (+ 2 autres pour les versions REST et SOAP) et un pour la partie EAR.

La création des archives (CompteEjb.jar, CompteEar.ear, CompteWeb.war, CompteSoap.war, CompteRest.war) 
peut se faire via gradle en appelant la tâche build sur le projet principal : 
`./gradlew build`

Pour utiliser l'exemple il suffit de déployer le fichier **CompteEar.ear** sur un serveur Jakarta EE 9 ou Jakarta EE 10. 
Le client Web est alors dans déployé dans */CompteWeb*.

## Documentation :

Java EE 7 (Oracle)
- Doc : http://docs.oracle.com/javaee/7
- Tutoriel : https://docs.oracle.com/javaee/7/tutorial
- API (javadoc) : http://docs.oracle.com/javaee/7/api
- Spécifications : https://www.oracle.com/java/technologies/javaee/javaeetechnologies.html#javaee7

Jave EE 8 (Oracle)
- Doc : https://javaee.github.io/glassfish/documentation
- Tutoriel : https://javaee.github.io/tutorial/
- API (javadoc) : https://javaee.github.io/javaee-spec/javadocs/
- Spécifications : https://www.oracle.com/java/technologies/javaee/javaeetechnologies.html#javaee8
- Serveurs compatibles : https://www.oracle.com/java/technologies/compatibility-jsp.html

Jakarta EE 8 (Fondation Eclipse)
- Doc : https://javaee.github.io/glassfish/documentation
- Tutoriel : https://javaee.github.io/tutorial/
- API (javadoc) : https://jakarta.ee/specifications/platform/8/apidocs/
- Spécifications : https://jakarta.ee/specifications
- Serveurs compatibles : https://jakarta.ee/compatibility/certification/8/

Jakarta EE 9 (Fondation Eclipse)
- Doc : https://jakarta.ee/resources/#documentation
- Tutoriel : https://eclipse-ee4j.github.io/jakartaee-tutorial/
- API (javadoc) : https://jakarta.ee/specifications/platform/9/apidocs/
- Spécifications : https://jakarta.ee/specifications
- Serveurs compatibles : 
    - https://jakarta.ee/compatibility/certification/9/
    - https://jakarta.ee/compatibility/certification/9.1/

Jakarta EE 10 (Fondation Eclipse)
- Doc : https://jakarta.ee/resources/#documentation
- API (javadoc) : https://jakarta.ee/specifications/platform/10/apidocs/
- Spécifications : https://jakarta.ee/specifications
- Serveurs compatibles : 
    - https://jakarta.ee/compatibility/certification/10/


