package fr.usmb.m2isc.javaee.comptes.web;

import java.io.IOException;
import java.util.List;

import jakarta.ejb.EJB;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import fr.usmb.m2isc.javaee.comptes.ejb.Operation;
import fr.usmb.m2isc.javaee.comptes.jpa.Compte;


/**
 * Servlet utilisee pour rechercher des comptes a partir d'un numero partiel.
 */
@WebServlet("/ChercherComptesServlet")
public class ChercherComptesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
	private Operation ejb;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChercherComptesServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String partialNumber = request.getParameter("partialNumber");
		List<Compte> cpts ;
		if (partialNumber != null && partialNumber.length() > 0 ) {
			cpts = ejb.findComptes("%"+partialNumber+"%");
		} else {
			cpts = ejb.findAllComptes();
		}
		request.setAttribute("comptes", cpts);
		request.getRequestDispatcher("/AfficherComptes.jsp").forward(request, response);		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
